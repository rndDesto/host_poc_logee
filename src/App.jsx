import Providers from "@/Providers";
import Partner from "@/Pages/Partner";
import styles from "./App.module.css";
function App() {

  return (
    <>
      <h1>HOST APPSS</h1>
      <div className={styles.connectToRoute}>
      <Providers />
      </div>

      <div className={styles.outsideRoute}>
        <p>Komponen diluar route</p>
        <Partner />
      </div>
    </>
  );
}

export default App;
