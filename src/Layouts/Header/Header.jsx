import { Link } from "@tanstack/react-location"
import styles from './Header.module.css'
import { usePageBaseProvider } from "@/Providers/PagebaseProvider";
const Header = () => {

  const {dataPageBase} = usePageBaseProvider()

  console.log("dataPageBase= ", dataPageBase);
  return (
    <div className={styles.link}>
      <Link to="/" activeOptions={{ exact: true }}>
        Home
      </Link>
      <Link to="/proforma" activeOptions={{ exact: true }}>
        Proforma
      </Link>
      <Link to="/brand" activeOptions={{ exact: true }}>
        Brand
      </Link>
      </div>
  )
}

export default Header