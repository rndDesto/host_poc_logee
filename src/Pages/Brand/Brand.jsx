import React, { Suspense } from "react";
const BrandPage = React.lazy(() => import("remoteSales/BrandPage"));


import ErrorBoundary from "@/Pages/ErrorBoundary";

const Brand = () => {
  return (
    <div>
        <ErrorBoundary fallback="Remote Brand Connection Refuse">
          <Suspense fallback={"connecting remote Brand"}>
            <BrandPage />
          </Suspense>
        </ErrorBoundary>
      </div>
  )
}

export default Brand