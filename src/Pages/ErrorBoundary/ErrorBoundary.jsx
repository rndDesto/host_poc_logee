import React from "react";
import PropTypes from 'prop-types';
 
// Error boundaries currently have to be a class component.
class ErrorBoundary extends React.Component {
  state = { 
    hasError: false, 
    error: null 
};

  static getDerivedStateFromError(error) {
    return {
      hasError: true,
      error,
    };
  }

  componentDidCatch(error,info){
    console.log(error,info)
  }

  render() {
    if (this.state.hasError) {
      return this.props.fallback;
    }
    return this.props.children;
  }
}

ErrorBoundary.defaultProps = {
    children: null,
    fallback:''
  };
  
  ErrorBoundary.propTypes = {
    children: PropTypes.node,
    fallback: PropTypes.string,
  };
  
 
export default ErrorBoundary;

