import { getCookie, getParentDomain } from "@logee-fe/utils";

const Home = () => {
  getCookie("tokenCookie")
  return (
    <div>
      <div>Home Host</div>

      <p>token cookie = {getCookie("tokenCookie")}</p>

      <button onClick={()=>removeCookie('tokenCookie', { path: '', domain: 'host-poc-logee.vercel.app' })}>hapus cookie</button>
    </div>
  );
};

export default Home;
