import React, { Suspense } from "react";
import ErrorBoundary from "@/Pages/ErrorBoundary";
import { useNavigate } from "@tanstack/react-location";


const KontainerPage = React.lazy(() => import("remoteProforma/KontainerPage"));


const Kontainer = () => {
    const navigate = useNavigate()
    const handleMantul=(values)=>{
        console.log("di triger dari remote dengan parameter ", values);
        navigate({ to: './proforma', replace: true,search:{mantul:'sssss'} })
    }
  return (
    <div>
        <ErrorBoundary fallback="Remote Proforma Connection Refuse">
          <Suspense fallback={"connecting remote proforma"}>
            <KontainerPage goTo={handleMantul}/>
          </Suspense>
        </ErrorBoundary>
      </div>
  )
}

export default Kontainer