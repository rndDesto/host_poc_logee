import React, { Suspense } from "react";
const PartnerPage = React.lazy(() => import("remoteSales/PartnerPage"));

import ErrorBoundary from "@/Pages/ErrorBoundary";

const Partner = () => {
  return (
    <div>
        <ErrorBoundary fallback="Remote Proforma Connection Refuse">
          <Suspense fallback={"connecting remote proforma"}>
            <PartnerPage />
          </Suspense>
        </ErrorBoundary>
      </div>
  )
}

export default Partner