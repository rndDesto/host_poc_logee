import React, { Suspense } from "react";
const ProformaPage = React.lazy(() => import("remoteProforma/ProformaPage"));

import ErrorBoundary from "@/Pages/ErrorBoundary";


const Proforma = () => {
  return (
    <div>
        <h1>Halaman Proforma</h1>
        <ErrorBoundary fallback="Remote Proforma Connection Refuse">
          <Suspense fallback={"connecting remote proforma"}>
            <ProformaPage />
          </Suspense>
        </ErrorBoundary>
      </div>
  )
}

export default Proforma