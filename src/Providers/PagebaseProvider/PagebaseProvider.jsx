/* eslint-disable react/prop-types */
import { createContext, useContext, useState } from "react";

const defaultValues = {
  dataPageBase: null,
  setDataPageBase:()=>{}
};

const PageBaseContext = createContext(defaultValues);

export const PagebaseProvider = ({ children }) => {
  const [dataPageBase, setDataPageBase] = useState({
    mantul:'cihuy'
  });
  return (
    <PageBaseContext.Provider value={{dataPageBase, setDataPageBase}}>
      {children}
    </PageBaseContext.Provider>
  );
};

export const usePageBaseProvider = () => {
  const context = useContext(PageBaseContext);
  if (!context) {
    throw new Error('usePageBaseProvider must be used within a PageBaseContext');
  }
  return context;
};