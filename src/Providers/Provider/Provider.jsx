import RouterProvider from "@/Providers/RouterProvider";
import { PagebaseProvider } from "@/Providers/PagebaseProvider";
import { SharedProvider } from "../SharedProvider/SharedProvider";
const Providers = () => {
  return (
    <SharedProvider.Provider>
      <PagebaseProvider>
        <RouterProvider />
      </PagebaseProvider>
    </SharedProvider.Provider>
  );
};

export default Providers;
