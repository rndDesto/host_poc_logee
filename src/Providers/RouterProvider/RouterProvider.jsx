import { routes } from '@/Routes'
import { Outlet, ReactLocation, Router, createHashHistory } from '@tanstack/react-location'
import Header from '@/Layouts/Header'
import Footer from '@/Layouts/Footer'
import Kontainer from '@/Pages/Kontainer';
 
// const hashHistory = createHashHistory()

// const location = new ReactLocation({
//   history: hashHistory,
// })


const location = new ReactLocation()

const RouterProvider = () => {
  return (
    <Router
        location={location}
        routes={routes}
        defaultPendingElement={<div>loading. . .</div>}
        defaultErrorElement={<div>error</div>}
      >
        <Header />
        <Outlet />
        <Kontainer />

        <Footer />
      </Router>
  )
}

export default RouterProvider