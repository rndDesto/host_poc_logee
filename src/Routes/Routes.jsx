const compose = ({ default: Component }) => <Component />;

export const routes = [
  {
    path: "/",
    element: async () => import("@/Pages/Home").then(compose)
  },
  {
    path: '/proforma',
    element: async () => import("@/Pages/Proforma").then(compose).catch((err)=>console.log("error gan", err))
  },
  {
    path: '/brand',
    element: async () => import("@/Pages/Brand").then(compose)
  },
];
