import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react-swc";
import federation from "@originjs/vite-plugin-federation";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), "");

  return {
    resolve: {
      alias: [{ find: "@", replacement: path.resolve(__dirname, "src") }],
    },
    plugins: [
      react(),
      federation({
        name: "app",
        remotes: {
          remoteProforma: env.REMOTE_PROFORMA,
          remoteSales: env.REMOTE_SALES,
        },
        shared: ["react", "react-dom", "@tanstack/react-location","@logee-fe/services","@logee-fe/utils"],
      }),
    ],
    server: {
      base: "/",
    },
    build: {
      modulePreload: false,
      target: "esnext",
      minify: false,
      cssCodeSplit: false,
    },
  };
});
